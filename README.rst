============
 Gamecrafty
============

Setup
=====

Basic development is via python + flask + heroku + mongodb

Heroku Setup
------------

One time machine setup:

1. Install the Heroku toolbelt: https://toolbelt.heroku.com/
2. Ensure git works

Setting up your heroku app (see https://devcenter.heroku.com/articles/quickstart):

1. heroku login
2. heroku apps:create
3. heroku addons:add mongohq:sandbox
4. heroku config:set SECRET_KEY='daeb7bed-cdc5-4200-9183-a3c6b36fa33f'
5. git push heroku master

If you want to push to the real site:

1. git remote add gamecraft git@heroku.com:gamecrafty.git
2. git push gamecraft master

Local Development Setup
-----------------------

If you want to run stuff locally:

1. Install virtualenv (it's very frequently already installed on your machine, if not download from http://www.virtualenv.org/en/latest/ )
2. Install mongodb (again, apt-get install mongodb or brew install mongodb might do the trick)

Setting up development:

1. virtualenv gamecraft-venv
2. . gamecraft-venv/bin/activate
3. pip install -r requirements.txt -r dev-requirements.txt
4. python setup.py develop

Testing:

1. tox (runs the tests, initially there will be one test but hopefully it will bring some friends)

Running (yay):

1. python -m gamecraft.app
