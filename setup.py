from setuptools import setup


setup(
    name="gamecraft",
    version="0.1",
    author="Gamecraft",
    url="https://bitbucket.org/gamecraft/gamecraft",
    packages=[
        "gamecraft",
        "gamecraft.tests",
    ],
    license="MIT",
    include_package_data=True,
)
