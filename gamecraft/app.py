import functools
import logging
import os
import re

import flask
from flask_debugtoolbar import DebugToolbarExtension
from flask.ext.compress import Compress
from flask.ext.heroku import Heroku
from flask.ext.mongoengine import MongoEngine
from flask.ext.wtf import (
    FileField,
    Form,
)

from flaskext.markdown import Markdown

import flask.ext.uploads

from raven.contrib.flask import Sentry

from werkzeug import secure_filename

import wtforms

import requests

from gamecraft import data

app = flask.Flask(__name__)
Markdown(app)
Compress(app)

def heroku():
    app.config.from_object('gamecraft.config.Production')
    Heroku(app)
    Sentry(app)
    # Map flask heroku config to flask mongoengine config
    app.config["MONGODB_SETTINGS"] = {
        "DB": app.config["MONGODB_DB"],
        "HOST": app.config["MONGODB_HOST"],
        "PORT": app.config["MONGODB_PORT"],
        "USERNAME": app.config["MONGODB_USER"],
        "PASSWORD": app.config["MONGODB_PASSWORD"],
    }
    MongoEngine(app)
    if not app.config["SECRET_KEY"]:
        app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY", None)
    for key, val in sorted(app.config.iteritems()):
        logging.info("Setting {!r}: {!r}".format(key, val))
    return app

ADMINS = (
    "micktwomey@gmail.com",
    "whykay@gmail.com",
    "roundcrisis@gmail.com",
)


@app.before_request
def authenticate():
    flask.g.email = None
    flask.g.authenticated = False
    flask.g.admin = False
    if "email" in flask.session:
        flask.g.email = flask.session["email"].lower()
        flask.g.authenticated = True
        if flask.g.email in ADMINS:
            flask.g.admin = True


def require_authentication(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        if not flask.g.authenticated:
            flask.abort(401)
        return fn(*args, **kwargs)
    return wrapper


def require_admin(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        if not flask.g.admin:
            flask.abort(401)
        return fn(*args, **kwargs)
    return wrapper


@app.route("/")
def index():
    return flask.render_template(
        "index.html",
        gamecrafts=[value for (key, value) in data.gamecrafts.iteritems()]
    )


@app.route("/favicon.ico")
def favicon():
    return flask.send_file(app.open_resource("static/favicon.ico"))


@app.route("/<gamecraft>/")
def gamecraft(gamecraft):
    if gamecraft not in data.gamecrafts:
        flask.abort(404)
    return flask.render_template(
        "gamecraft.html",
        gamecraft=data.gamecrafts[gamecraft]
    )


@app.route("/<gamecraft>/<game>/")
def game(gamecraft, game):
    gc = data.gamecrafts[gamecraft]
    game = data.Game.objects.get(name=game)
    return flask.render_template("game.html", gamecraft=gc, game=game)


def generate_name_from_game_title(gamecraft, title):
    name = re.sub(r"[^a-zA-Z0-9]", "-", title.strip().lower())
    return "{}-{}".format(gamecraft, name)


class GameForm(Form):
    title = wtforms.TextField(
        "Game's title",
        validators=[wtforms.validators.DataRequired()],
        description="The name of your game (or your team's name)."
    )
    owners = wtforms.HiddenField(
        "Game owners",
        validators=[wtforms.validators.DataRequired()],
        description="Folks who can edit the game details and upload screenshots. You can add more later. Use space to separate the addresses. Hopefully this is your trusted team."
    )
    description = wtforms.TextAreaField(
        "Description",
        validators=[wtforms.validators.DataRequired()],
        description="Describe your game. You can add any links you want here. Markdown is used to format the text. Screenshots can be uploaded on the game page once it's created."
    )
    gamecraft = wtforms.HiddenField("gamecraft", validators=[])


class GameCreateForm(GameForm):
    def validate_title(form, field):
        name = generate_name_from_game_title(
            form.data["gamecraft"],
            field.data
        )
        if data.Game.objects.filter(name=name).count():
            raise wtforms.validators.ValidationError('Name is already used in this GameCraft')


def parse_owners(owners):
    return [s.strip().lower() for s in re.split(r"[, :;]", owners) if s.strip()]


@require_authentication
@app.route("/<gamecraft>/create/", methods=["GET", "POST"])
def create_game(gamecraft):
    """Create a new game

    The owner is set here

    """
    form = GameCreateForm(owners=flask.g.email, gamecraft=gamecraft)
    if form.validate_on_submit():
        name = generate_name_from_game_title(gamecraft, form.title.data)
        logging.info("Creating game with {!r} from {!r}".format(form.data, flask.g.email))
        if data.Game.objects.filter(name=name).count():
            return flask.render_template(
                "create_game.html",
                gamecraft=data.gamecrafts[gamecraft],
                form=form
            )
        game = data.Game(
            title=form.title.data,
            name=name,
            description=form.description.data,
            owners=parse_owners(form.owners.data),
            gamecraft=gamecraft,
        )
        game.save()
        logging.info("Created {!r} by {!r}".format(game, flask.g.email))
        return flask.redirect(flask.url_for("game", gamecraft=gamecraft, game=game.id))
    return flask.render_template(
        "create_game.html",
        gamecraft=data.gamecrafts[gamecraft],
        form=form
    )


@require_authentication
@app.route("/<gamecraft>/<game>/edit/", methods=["GET", "POST"])
def edit_game(gamecraft, game):
    game = data.Game.objects.get(gamecraft=gamecraft, name=game)
    form = GameForm(title=game.title, name=game.name, description=game.description, owners=" ".join(game.owners), gamecraft=gamecraft)
    if form.validate_on_submit():
        logging.info("Updating game {!r} with {!r} from {!r}".format(game, vars(form), flask.g.email))
        game.title = form.title.data
        game.description = form.description.data
        game.owners = parse_owners(form.owners.data)
        game.save()
        logging.info("Updated {!r} by {!r}".format(game, flask.g.email))
        return flask.redirect(flask.url_for("game", gamecraft=gamecraft, game=game.id))
    return flask.render_template("edit_game.html", gamecraft=data.gamecrafts[gamecraft], game=game, form=form)


class ScreenshotForm(Form):
    screenshot = FileField("Screenshot", validators=[wtforms.validators.DataRequired()], description="Upload an image directly")
    title = wtforms.TextField("Title", validators=[wtforms.validators.DataRequired()], description="Give your screenshot a nice title")
    description = wtforms.TextAreaField("Description", validators=[], description="Describe your screenshot.")


@require_authentication
@app.route("/<gamecraft>/<game>/upload_screenshot/", methods=["GET", "POST"])
def upload_screenshot(gamecraft, game):
    """Upload a screenshot to S3 and update the game with a link

    Only owners can do this
    """
    gamecraft = data.gamecrafts[gamecraft]
    game = data.Game.objects.get(name=game, gamecraft=gamecraft.id)
    form = ScreenshotForm(title="Screenshot {}".format(len(game.screenshots) + 1))
    if form.validate_on_submit():
        logging.info("Uploading screenshot {!r} with {!r} from {!r}".format(game, vars(form), flask.g.email))

        filename = secure_filename(form.screenshot.data.filename)
        logging.info("Got filename {!r} from {!r}".format(filename, flask.g.email))

        imgur = data.upload_to_imgur(
            data=form.screenshot.data,
            title=form.title.data,
            description=form.title.description,
        )
        imgur.save()

        # key = "/".join(("screenshots", gamecraft.id, game.name, filename))
        # logging.info("Using key {}".format(key))

        # s3_key = data.get_s3_key(key)
        # headers = {}
        # if form.screenshot.data.mimetype:
            # headers["Content-Type"] = form.screenshot.data.mimetype
        # s3_key.set_contents_from_file(form.screenshot.data, policy="public-read", headers=headers)

        screenshot = data.ScreenShot(
            title=form.title.data,
            description=form.description.data,
            url=imgur.link,
            imgur=imgur,
        )
        screenshot.save()
        game.update(add_to_set__screenshots=[screenshot])
        game.save()

        logging.info("Updated {!r} from {!r}".format(game, flask.g.email))
        return flask.redirect(flask.url_for("game", gamecraft=gamecraft.id, game=game.id))
    return flask.render_template("upload_screenshot.html", gamecraft=gamecraft, game=game, form=form)

class DeleteScreenshotForm(Form):
    confirm = wtforms.TextField("Confirm Deletion?", validators=[wtforms.validators.DataRequired()], description="Type 'yes' in here to confirm")

@app.route("/<gamecraft>/<game>/delete_screenshot/<screenshot>/", methods=["GET", "POST"])
@require_admin
def delete_screenshot(gamecraft, game, screenshot):
    gamecraft = data.gamecrafts[gamecraft]
    game = data.Game.objects.get(name=game, gamecraft=gamecraft.id)
    form = DeleteScreenshotForm()
    screenshot = data.ScreenShot.objects.get(id=screenshot)
    if form.validate_on_submit() and form.confirm.data.lower() == "yes":
        logging.info("Deleting screenshot {!r} by {!r}".format(screenshot, flask.g.email))
        game.screenshots = [sc for sc in game.screenshots if sc != screenshot]
        game.save()
        screenshot.delete()
        return flask.redirect(flask.url_for("game", gamecraft=gamecraft.id, game=game.id))
    return flask.render_template("delete_screenshot.html", gamecraft=gamecraft, game=game, form=form, screenshot=screenshot)


class DeleteGameForm(Form):
    confirm = wtforms.TextField("Confirm Deletion?", validators=[wtforms.validators.DataRequired()], description="Type 'yes' in here to confirm")


@app.route("/<gamecraft>/<game>/delete/", methods=["GET", "POST"])
@require_admin
def delete_game(gamecraft, game):
    gamecraft = data.gamecrafts[gamecraft]
    game = data.Game.objects.get(name=game, gamecraft=gamecraft.id)
    form = DeleteGameForm()
    if form.validate_on_submit() and form.confirm.data.lower() == "yes":
        logging.info("Deleting game {!r} by {!r}".format(game, flask.g.email))
        game.delete()
        return flask.redirect(flask.url_for("gamecraft", gamecraft=gamecraft.id))
    return flask.render_template("delete_game.html", gamecraft=gamecraft, game=game, form=form)


@app.route("/auth/whoami/")
def whoami():
    return flask.jsonify({
        "email": flask.g.get("email", None)
    })


@app.route("/auth/login/", methods=["POST"])
def login():
    # TODO: nail url_root
    data = {'assertion': flask.request.form['assertion'], 'audience': flask.request.url_root}
    resp = requests.post('https://verifier.login.persona.org/verify', data=data, verify=True)

    # Did the verifier respond?
    if resp.ok:
        # Parse the response
        verification_data = flask.json.loads(resp.content)

        # Check if the assertion was valid
        if verification_data['status'] == 'okay':
            email = verification_data['email']
            flask.session["email"] = email.lower().strip()
            logging.info("Logged in as {!r}".format(email))
            return ("You are logged in", 200)

    # Assume there was a problem talking to Persona at this point
    flask.abort(500)


@app.route("/auth/logout/", methods=["POST"])
def logout():
    flask.session.clear()
    return ("", 204)


@app.route("/export.json")
@require_admin
def export():
    """Dump out all the data as a nice json blob

    """
    return flask.jsonify(**data.export())


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logging.info("Using config gamecraft.config.Development")
    app.config.from_object('gamecraft.config.Development')
    MongoEngine(app)
    DebugToolbarExtension(app)
    app.run(host="0.0.0.0", debug=True)
