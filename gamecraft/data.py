import collections
import json
import logging
import os
import sys

import mongoengine
from mongoengine.fields import (
    ListField,
    StringField,
    URLField,
    ReferenceField,
    IntField,
)

import requests

import yaml

LOG = logging.getLogger(__name__)


class ScreenShot(mongoengine.Document):
    description = StringField()
    key = StringField()
    url = URLField()
    title = StringField()
    imgur = ReferenceField("Imgur")

    meta = {
        'ordering': ['title']
    }

    def __repr__(self):
        return "<ScreenShot key={self.key!r} url={self.url!r} title={self.title!r}".format(self=self)

    def __str__(self):
        return repr(self)

    def to_dict(self):
        screenshot = json.loads(self.to_json())
        screenshot["thumbnail_url"] = self.thumbnail_url
        if self.imgur:
            screenshot["imgur"] = self.imgur.to_dict()
        return screenshot

    @property
    def thumbnail_url(self):
        try:
            if not self.imgur:
                return self.url
            return self.imgur.thumbnail_url
        except Exception:
            LOG.exception("Problem generating thumbnail url from {!r}".format(self))
            return self.url


class Imgur(mongoengine.Document):
    imgurid = StringField()
    link = URLField()
    image_type = StringField()
    width = IntField()
    height = IntField()
    description = StringField()
    title = StringField()

    def get_thumbnail(self, size):
        """Returns an imgur thumbnail

        Sizes: s, b, t, m, l, h

        """
        prefix, suffix = os.path.splitext(self.link)
        return "".join((prefix, size, suffix))

    @property
    def thumbnail_url(self):
        return self.get_thumbnail("m")

    @property
    def small_square_url(self):
        return self.get_thumbnail("s")

    def to_dict(self):
        imgur = json.loads(self.to_json())
        imgur["thumbnail_url"] = self.thumbnail_url
        imgur["small_square_url"] = self.small_square_url
        return imgur


def upload_to_imgur(data, title="", description=""):
    response = requests.post(
        "https://api.imgur.com/3/image",
        files={'image': data},
        data={
            "title": title,
            "description": description,
        },
        headers={
            "Authorization": "Client-ID 8f8a527f40d4415",
        },
    )
    assert response.status_code == 200, response
    data = response.json()["data"]
    return Imgur(
        imgurid=data["id"],
        link=data["link"],
        image_type=data["type"],
        width=data["width"],
        height=data["height"],
        description=data["description"],
        title=data["title"],
    )


class Game(mongoengine.Document):
    """An entry in gamecraft

    """
    owners = ListField(StringField())
    title = StringField()
    name = StringField(primary_key=True)
    description = StringField()
    screenshots = ListField(ReferenceField(ScreenShot))
    gamecraft = StringField()

    meta = {
        'ordering': ['name']
    }

    def __repr__(self):
        return "<Game title={self.title!r} name={self.name!r}>".format(self=self)

    def __str__(self):
        return repr(self)

    def to_dict(self):
        game = json.loads(self.to_json())
        game["screenshots"] = [s.to_dict() for s in self.screenshots]
        return game

    def get_gamecraft(self):
        """Returns the GameCraft object associated with this game

        """
        return gamecrafts[self.gamecraft]

    def can_edit(self, email, is_admin):
        # If you are an admin you can edit
        if is_admin:
            return True
        # Is this is an editable gamecraft?
        if getattr(self.get_gamecraft(), "readonly", False):
            return False
        # Finally check if the caller is an owner of the game
        # return email in self.owners
        # Throw open to anyone who is logged in
        return True if email else False

    def can_delete(self, email, is_admin):
        if is_admin:
            return True
        return False


class GameCraft(object):
    """Represents a gamecraft

    """
    def __init__(self, gamecraft):
        self.data = gamecraft

    def __getattr__(self, attr):
        try:
            return self.data[attr]
        except KeyError:
            raise AttributeError(attr)

    def to_dict(self):
        gc = dict(self.data)
        gc["when"] = gc["when"].isoformat()
        gc["games"] = []

        for game in self.games:
            gc["games"].append(game.to_dict())

        return gc

    @property
    def games(self):
        for game in Game.objects.order_by("+title", "+name"):
            if game.gamecraft == self.id:
                yield game
            elif self.id == "london-august-2013" and game.gamecraft is None:
                game.gamecraft = self.id
                game.save()
                yield game

gamecrafts = collections.OrderedDict(
    (key, GameCraft(val))
    for (key, val) in
    sorted(
        yaml.load(
            open(
                os.path.join(os.path.dirname(__file__), 'gamecrafts.yaml')
            )
        ).iteritems(),
        key=lambda x: (x[1]["when"], x[1]["id"]),
        reverse=True,
    )
)


def export():
    results = {}
    for id, gamecraft in gamecrafts.iteritems():
        results[id] = gamecraft.to_dict()
    return results

if __name__ == '__main__':
    import mongoengine
    mongoengine.connect("gamecraft")
    json.dump(export(), sys.stdout)
