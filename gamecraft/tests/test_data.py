from gamecraft import data


def test_gamecrafts():
    """Ensure all the gamecrafts have all the requisite bits

    """
    for gamecraft_id, gamecraft in data.gamecrafts.iteritems():
        assert gamecraft_id == gamecraft.id
        for key in "id title readonly when description".split():
            assert key in gamecraft.data
            assert gamecraft.data[key] is not None


def test_export():
    assert data.export()
