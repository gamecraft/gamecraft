from flask.ext.mongoengine import MongoEngine

import pytest

import gamecraft.app
import gamecraft.data


@pytest.fixture
def app():
    gamecraft.app.app.config.from_object('gamecraft.config.Test')
    MongoEngine(gamecraft.app.app)
    gamecraft.data.Game.drop_collection()
    return gamecraft.app.app.test_client()
