"""Tests the app

"""


def test_index(app):
    response = app.get("/")
    assert "<h1>Global GameCraft</h1>" in response.data


def test_gamecraft(app):
    response = app.get("/telford-gamecraft-2013/")
    assert "<h1>Telford GameCraft 2013</h1>" in response.data
