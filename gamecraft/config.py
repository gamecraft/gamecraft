"""Configuration

On Heroku env variables are used, otherwise these settings are used

So, this is likely only ever used in development and test :)

"""


class Production(object):
    DEBUG = False


class Development(object):
    SECRET_KEY = "8176d4fe-9245-4c89-8bda-2847f36d5e51"
    MONGODB_SETTINGS = {
        "DB": "gamecraft",
        "HOST": "localhost",
    }
    IMGUR_CLIENT_ID = "8f8a527f40d4415"
    DEBUG_TB_ENABLED = True
    DEBUG_TB_PANELS = (
        'flask_debugtoolbar.panels.versions.VersionDebugPanel',
        'flask_debugtoolbar.panels.timer.TimerDebugPanel',
        'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
        'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
        'flask_debugtoolbar.panels.config_vars.ConfigVarsDebugPanel',
        'flask_debugtoolbar.panels.template.TemplateDebugPanel',
        "flask.ext.mongoengine.panels.MongoDebugPanel",
        'flask_debugtoolbar.panels.logger.LoggingPanel',
        'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
    )
    DEBUG = True


class Test(object):
    SECRET_KEY = "sekret-key"
    MONGODB_SETTINGS = {
        "DB": "gamecraft_tests",
        "HOST": "localhost",
    }
    IMGUR_CLIENT_ID = "no-id"
