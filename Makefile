export MONGOHQ_URL=$(shell heroku config:get MONGOHQ_URL)
MONGO_USERNAME=$(shell echo $(MONGOHQ_URL) | python -c "import urlparse, sys; print urlparse.urlparse(sys.stdin.read().strip()).username")
MONGO_PASSWORD=$(shell echo $(MONGOHQ_URL) | python -c "import urlparse, sys; print urlparse.urlparse(sys.stdin.read().strip()).password")
MONGO_HOSTNAME=$(shell echo $(MONGOHQ_URL) | python -c "import urlparse, sys; print urlparse.urlparse(sys.stdin.read().strip()).hostname")
MONGO_PORT=$(shell echo $(MONGOHQ_URL) | python -c "import urlparse, sys; print urlparse.urlparse(sys.stdin.read().strip()).port")
MONGO_DB=$(shell echo $(MONGOHQ_URL) | python -c "import urlparse, sys; print urlparse.urlparse(sys.stdin.read().strip()).path[1:]")

VIRTUALENV=$(CURDIR)/gamecraft-venv
VIRTUALENV_BIN=$(VIRTUALENV)/bin
INSTALLED=$(VIRTUALENV)/installed.txt

PYTHON=$(VIRTUALENV_BIN)/python
PIP=$(VIRTUALENV_BIN)/pip
FLAKE8=$(VIRTUALENV_BIN)/flake8
TOX=$(VIRTUALENV_BIN)/tox

export IMGUR_CLIENT_ID=$(shell heroku config:get IMGUR_CLIENT_ID)
export SECRET_KEY=$(shell heroku config:get SECRET_KEY)
export SENTRY_DSN=$(shell heroku config:get SENTRY_DSN)

all: test run

# Runs using heroku foreman to simulate live site
foreman:
	@echo "MONGOHQ_URL: $(MONGOHQ_URL)"
	@echo "IMGUR_CLIENT_ID: $(IMGUR_CLIENT_ID)"
	@echo "SECRET_KEY: $(SECRET_KEY)"
	@echo "SENTRY_DSN: $(SENTRY_DSN)"
	@echo
	@echo "NB: This is using real data!"
	@echo
	foreman start

run:
	$(PYTHON) -m gamecraft.app

push:
	git push origin master

deploy: push
	git push heroku master

dump-from-heroku:
	mongodump --host $(MONGO_HOSTNAME) -port $(MONGO_PORT) --username $(MONGO_USERNAME) --password $(MONGO_PASSWORD) --db $(MONGO_DB)

restore-from-dump:
	mongorestore --db gamecraft --drop dump/$(MONGO_DB)

import-from-heroku: dump-from-heroku restore-from-dump

json:
	$(PYTHON) -m gamecraft.data | jq .

lint:
	git diff | $(FLAKE8) --diff
	git diff --cached | $(FLAKE8) --diff

test: $(TOX)
	$(TOX)

clean:
	rm -rf $(CURDIR)/build
	rm -rf $(CURDIR)/dist
	rm -rf $(CURDIR)/dump
	find gamecraft -type f -name '*.pyc' | xargs rm -v

realclean: clean
	rm -rf $(CURDIR)/.tox
	rm -rf $(VIRTUALENV)
	rm -rf $(CURDIR)/gamecraft.egg-info

$(TOX): $(INSTALLED)

$(INSTALLED): requirements.txt dev-requirements.txt $(PYTHON) $(CURDIR)/setup.py
	$(PIP) install -r requirements.txt -r dev-requirements.txt -U
	$(PIP) list > $(INSTALLED)
	$(PYTHON) $(CURDIR)/setup.py develop
	$(TOX) --recreate py27

install: $(INSTALLED)

$(PYTHON):
	virtualenv $(VIRTUALENV)
